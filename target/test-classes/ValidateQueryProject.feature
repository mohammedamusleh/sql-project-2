Feature: As a QE, validating that I can connect to database
  @st
  Scenario: Validating the db connection
    Given User is able to connect to database
    When User enters "SELECT first_name,last_name FROM employees WHERE manager_id = (SELECT employee_id FROM employees WHERE first_name = 'Payam' )" as a query
    And User should see the data displayed as below

      |Jason|Mallin|
      |Michael|Rogers|
      |Ki|Gee|
      |Hazel|Philtanker|
      |Kelly|Chung|
      |Jennifer|Dilly|
      |Timothy|Gates|
      |Randall|Perkins|

package stepDef;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.testng.Assert;
import utils.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class StepDefinition {
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    @Given("User is able to connect to database")
    public void user_is_able_to_connect_to_database() {
        connection = DBUtil.createDBConnection();
    }

    @When("User enters {string} as a query")
    public void userEntersAsAQuery(String query) throws SQLException {
        statement = connection.createStatement();
        resultSet = statement.executeQuery(query);
    }

    @And("User should see the data displayed as below")
    public void userShouldSeeTheDataDisplayedAsBelow(DataTable dataTable) throws SQLException {
        List<String> expectedList = new ArrayList<>(dataTable.asList());
        List<String> actualList = DBUtil.getQueryResultList(resultSet);
        Assert.assertEquals(actualList, expectedList);
    }
}


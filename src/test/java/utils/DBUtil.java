package utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBUtil {
    private static String url = "jdbc:oracle:thin:@tgbatch-3.cup7q3kvh5as.us-east-2.rds.amazonaws.com:1521/ORCL";
    private static String username = "techglobal";
    private static String password = "TechGlobal123!";
    private static String query = "SELECT first_name,last_name FROM employees WHERE manager_id = (SELECT employee_id FROM employees WHERE first_name = 'Payam' )";

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    public static Connection createDBConnection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
            System.out.println("User connected to the database");
        } catch (SQLException e) {
            System.out.println("Database connection if failed");
            e.printStackTrace();
        }
        return connection;
    }

    public static void executeQuery(String query) {
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            resultSet = statement.executeQuery(query);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getQueryResultList(ResultSet query) {
        List<String> actualList = new ArrayList();
        resultSet = query;


            try {
                while (resultSet.next()) {
                    actualList.add(resultSet.getString(1));
                    actualList.add(resultSet.getString(2));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            return actualList;
        }
}